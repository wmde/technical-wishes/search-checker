This script performs calls to the search api of different mediawiki instances.  Currently used to
test our adaption to the template search.

Installation
===

  pip install -r requirements.txt

Usage
===

  python3 search-check.py

Debugging is printed to the console, and full results are stored as `./output.csv` .
