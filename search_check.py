#!/usr/bin/env python
# checks for results of different search api queries
#
# Usage:
#    search-check.py [options]
#
# Options:
#    -h --help           Show this screen
#    --concurrency=<num> Maximum number of threads to run  [default: 10]

# LIBRARIES
from concurrent.futures import ThreadPoolExecutor
import csv  # storing in csv file
import re
import requests  # api calls

# VARIABLES
inputfile = 'search_api_tests.csv'
outputfile = 'output.csv'

concurrency = 5

user_agent = 'Search Checker 0.1 <wikimedia-de-tech@wikimedia.de>'
headers = {
    'user-agent': user_agent,
}

url_formats = {
    'prefixsearch': 'https://{wiki}/w/api.php?action=query&'
                    'generator=prefixsearch&gpssearch={query}'
                    '&gpsnamespace=10&gpslimit={limit}',
    'cirrussearch': 'https://{wiki}/w/api.php?action=query'
                    '&generator=search&gsrsearch={query}'
                    '&gsrprop=redirecttitle'
                    '&redirects=1&gsrnamespace=10&gsrlimit={limit}',
}


def run():
    rows = load_input()
    results = process(rows)
    write_results(results)


def load_input():
    # read csv file
    with open(inputfile) as csvDataFile:
        # TODO: DictReader would simplify the parser.
        reader = csv.reader(csvDataFile)
        next(reader)  # skip the first row with the titles
        rows = list(reader)

    return rows


def process(rows):
    with ThreadPoolExecutor(max_workers=concurrency) as executor:
        future = executor.map(process_row, rows)
        out = list(future)

    return out


def process_row(row):
    wiki = row[0]
    search_term = row[1]
    expected_results = row[2]
    expected_list = expected_results.splitlines()

    cirrus_matches_asterisk = get_cirrus_matches(wiki, search_term, 20, True)
    cirrus_matches = get_cirrus_matches(wiki, search_term, 20, False)
    prefix_matches = get_prefix_matches(wiki, search_term, 20)

    apis = {
        # all filled up with prefix
        'Cirrus 1 fixed prefix': EvalSearch(prefix_matches, cirrus_matches_asterisk, 1, []),
        'Cirrus 1 fixed prefix w matches': EvalSearch(prefix_matches, cirrus_matches_asterisk, 1, expected_list),
        'Cirrus 3 fixed prefix': EvalSearch(prefix_matches, cirrus_matches_asterisk, 3, []),
        'Cirrus 3 fixed prefix w matches': EvalSearch(prefix_matches, cirrus_matches_asterisk, 3, expected_list),
        'Cirrus no asterisk': EvalSearch(prefix_matches, cirrus_matches, 1, []),
        'Cirrus no asterisk w matches': EvalSearch(prefix_matches, cirrus_matches, 1, expected_list),
        # pure query results
        'Prefixsearch only': EvalSearch(prefix_matches, [], 10, []),
        'Prefixsearch w matches': EvalSearch(prefix_matches, [], 10, expected_list),
        'Cirrus only': EvalSearch([], cirrus_matches_asterisk, 0, []),
        'Cirrus only w matches': EvalSearch([], cirrus_matches_asterisk, 0, expected_list),
    }

    out_row = {
        'wiki id': wiki,
        'search term': search_term,
        'expected results': expected_results,
    }

    for label, api, in apis.items():
        results = api.call()
        out_row[label] = results

    return out_row


# generate API queries simulating our search and return search result
def call_api(api):
    api += '&format=json&formatversion=2'
    json = requests.get(api).json()
    return json


def should_append_star(query, asterisk):
    last_char = query[-1]
    return (last_char.isalpha() or last_char.isnumeric()) and asterisk


def get_cirrus_matches(wiki, query, limit, asterisk):
    if should_append_star(query, asterisk):
        query = query + '*'

    json = call_api(url_formats['cirrussearch'].format(wiki=wiki, query=query, limit=limit))
    results = []

    if json.get('query', False) and len(json['query']['pages']):
        results = sorted(json['query']['pages'], key=lambda x: x['index'])

    return results


def get_prefix_matches(wiki, query, limit):
    json = call_api(url_formats['prefixsearch'].format(wiki=wiki, query=query, limit=limit))
    results = []

    if json.get('query', False) and len(json['query']['pages']):
        results = sorted(json['query']['pages'], key=lambda x: x['index'])

    return results


class EvalSearch:
    def __init__(self, prefix_matches, cirrus_matches, prefix_num, expected_results):
        self.prefix_matches = prefix_matches
        self.cirrus_matches = cirrus_matches
        self.prefix_num = prefix_num
        self.expected_results = expected_results

    @staticmethod
    def already_has_page(old_pages, new_page):
        already_found = False
        for old_page in old_pages:
            if old_page.get('title', False) and (new_page['pageid'] == old_page['pageid'] or (new_page.get('redirecttitle', False) and new_page['redirecttitle'] == old_page['title'] ) ):
                already_found = True
                break

        return already_found

    def call(self):
        limit = 10
        search_result = ''

        results_pages = []

        pre_prefix_matches = self.prefix_matches[:self.prefix_num]
        post_prefix_matches = self.prefix_matches[self.prefix_num:]

        for entry in pre_prefix_matches:
            if entry.get('title', False):
                results_pages.append(entry)

        cirrus_matches = self.cirrus_matches[:limit]

        for entry in cirrus_matches:
            if len(results_pages) == limit:
                break
            if not self.already_has_page(results_pages, entry):
                results_pages.append(entry)

        for entry in post_prefix_matches:
            if len(results_pages) == limit:
                break
            if not self.already_has_page(results_pages, entry):
                results_pages.append(entry)

        for entry in results_pages:
            redirect = ''
            if entry.get('redirecttitle', False):
                redirect = ' (redirected from ' + entry['redirecttitle'] + ') '
            search_result += entry['title'] + redirect + ',\n'

        if len(self.expected_results):
            matches_expected = ''
            for expected in self.expected_results:
                i = 0
                found = False

                for entry in results_pages:
                    i += 1
                    if entry['title'][entry['title'].find(':')+1:].lower() == expected.lower():
                        found = True
                        matches_expected += expected + ' Found at: ' + str(i) + '\n'

                if not found:
                    matches_expected += expected + ' Not Found,\n'

            return matches_expected

        return search_result


def write_results(results):
    with open(outputfile, 'w', newline='') as file:
        keys = list(results[0].keys())
        csvwriter = csv.DictWriter(file, keys)
        csvwriter.writeheader()

        # TODO: restore ability to create and write example api queries

        # store result in output file
        for row in results:
            csvwriter.writerow(row)

        print('Done. Have a look at ' + outputfile + ' for the results!')


if __name__ == '__main__':
    run()
