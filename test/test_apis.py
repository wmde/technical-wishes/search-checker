from search_check import *


def test_new_api_regex():
    api = NewVe()
    assert api._should_append_star('foo')
    assert not api._should_append_star('foo ')


def test_new_api_url():
    expected = 'https://de.wikipedia.org/w/api.php?action=query' \
                    '&list=search&srsearch=foo*' \
                    '&srprop=redirecttitle' \
                    '&ppprop=disambiguation&redirects=true&srnamespace=10&srlimit=10&&formatversion=2&prop=info|pageprops'
    result = NewVe()._build_search_api('de', 'foo')
    assert expected == result
